#ifndef SNAKE_H
#define SNAKE_H

#include "snake_video.h"
#include "snake_list.h"
#include "snake_consts.h"
#include "snake_input.h"

typedef struct setting {
	int rows;
	int cols;
} Settings;

int getInputPlayer(int *cmd);
int inputToCommand(char c);
int checkInput(int input, int lastInput);
int updateGrid(int **grid, list *snake, int input);
void place_fruit(int **grid);
void place_walls(int **grid);

int load_config(Settings *sets);

#endif
