#include "snake_video.h"
#include "SDL2/SDL.h"
#include <stdio.h>	/* for fprintf() */
#include <stdlib.h>	/* for atexit() */ 

#define SCREEN_WIDTH 640*2
#define SCREEN_HEIGHT 480*2
#define SCREEN_BPP 32

SDL_Window *screen;

void initVideo(void) {
	if(SDL_Init(SDL_INIT_VIDEO) != 0) {
		fprintf(stderr,
			"\nUnable to initialize SDL:  %s\n",
			SDL_GetError()
		);
		exit(-1);
	}
	atexit(SDL_Quit);

	screen = SDL_CreateWindow("Snake",
                          SDL_WINDOWPOS_CENTERED,
                          SDL_WINDOWPOS_CENTERED,
                          SCREEN_WIDTH, SCREEN_HEIGHT,
                          SDL_WINDOW_OPENGL);


	if(screen == NULL)
		exit(-1);
}

void drawGrid(int **grid, int rows, int cols) {
	SDL_Rect pos;
	SDL_Surface *surf = SDL_GetWindowSurface(screen);
	int width, height;
	pos.x = 0;
	pos.y = 0;
	SDL_GetWindowSize(screen, &width, &height);
	pos.w = width / cols;
	pos.h = height / rows;
	int i, j;
	Uint32 floor = 	SDL_MapRGB(surf->format, 0, 0, 0);
	Uint32 everythingelse = SDL_MapRGB(surf->format, 255, 255, 0);
	Uint32 fruit = SDL_MapRGB(surf->format, 255, 6, 6);
	for(i = 0; i < rows; ++i) {
		for(j = 0; j < cols; ++j) {
			pos.x = j*pos.w;
			pos.y = i*pos.h;
			if(grid[j][i] == FLOOR)
				SDL_FillRect(surf, &pos, floor);
			
			else if(grid[j][i] == FRUIT)
				SDL_FillRect(surf, &pos, fruit);
			else
				SDL_FillRect(surf, &pos, everythingelse);
		}
	}
	SDL_UpdateWindowSurface(screen);
}

void closeVideo(void) {
	SDL_DestroyWindow(screen);
	SDL_Quit();
}

