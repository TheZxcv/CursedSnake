#include <ncurses.h>
#include <unistd.h>

#include "snake_video.h"

void initVideo(void) {
	initscr();
	noecho();
	curs_set(FALSE);
}

void drawGrid(int **grid, int rows, int cols) {
	clear();
	int i, j;
	for(i = 0; i < rows; ++i) {
		for(j = 0; j < cols; ++j)
			mvprintw(i, j, "%c", getCharBlock(grid[j][i]));
//			putchar(getCharBlock(grid[j][i]));
//		putchar('\n');
	}
	refresh();
}

void closeVideo(void) {
	endwin();
}
