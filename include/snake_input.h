#ifndef SNAKE_INPUT_H
#define SNAKE_INPUT_H

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <unistd.h>

#include "snake_consts.h"

void initInput(void);
int rawInput(void);
int hasInput(void);

#endif
