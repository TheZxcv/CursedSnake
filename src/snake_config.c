#include "snake.h"
#include <libconfig.h>

int load_config(Settings *sets) {
	config_t cfg;
	
	/* Initialization */
	config_init(&cfg);

	/* Read the file. If there is an error, report it and exit. */
	if (!config_read_file(&cfg, CONFIG_FILE)) {
		printf("\n%s:%d - %s", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
		return -1;
	}

	/* Get the configuration file name. */
	if (!config_lookup_int(&cfg, "rows", &sets->rows)) {
		printf("\nNo 'rows' setting in configuration file.");
		return -1;
	}
	if (!config_lookup_int(&cfg, "cols", &sets->cols)) {
		printf("\nNo 'cols' setting in configuration file.");
		return -1;
	}

	config_destroy(&cfg);
	return 1;
}
