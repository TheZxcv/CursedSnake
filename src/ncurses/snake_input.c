#include "snake_input.h"

fd_set rfds;
struct timeval tv;

void initInput(void) {
	FD_ZERO(&rfds);
	FD_SET(STDIN_FILENO, &rfds);
	tv.tv_sec = 0;
	tv.tv_usec = 50000;
}

int rawInput(void) {
	char c;
	while(!isalpha(c = getchar()));
		return c;

//	return EOF;
}

int hasInput(void) {
	if(select(STDIN_FILENO + 1, &rfds, NULL, NULL, &tv) == -1)
		fprintf(stderr, "Something wrong happened\n");
	int ret = FD_ISSET(STDIN_FILENO, &rfds);

	FD_ZERO(&rfds);
	FD_SET(STDIN_FILENO, &rfds);
	tv.tv_sec = 0;
	tv.tv_usec = 500000;

	return ret; 
}
