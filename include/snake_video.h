#ifndef SNAKE_VIDEO_H
#define SNAKE_VIDEO_H

#include "snake_consts.h"

void initVideo(void);
void drawGrid(int **grid, int rows, int cols);
void closeVideo(void);

#endif
