#ifndef SNAKE_LIST_H
#define SNAKE_LIST_H

#include <stdio.h>
#include <stdlib.h>

typedef struct point {
	int x;
	int y;
} point;

typedef struct list {
	int length;
	struct node *head;
	struct node *tail;
} list;

struct node {
	point *p;
	struct node *left;
};

list *new_list(point *h, point *t);
void add_on_head(list *l, point *p);
void remove_from_tail(list *l);
void free_node(struct node *n);

#endif
