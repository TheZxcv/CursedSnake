#ifndef SNAKE_CONSTS_H
#define SNAKE_CONSTS_H

#define TRUE 1
#define FALSE 0

#define DEFAULT_ROWS 20
#define DEFAULT_COLS 80
#define CONFIG_FILE "config.cfg"


enum { FLOOR=0, SNAKE, FRUIT, WALL, UNKNOWN_TILE};
enum { NONE=0, UP, DOWN, LEFT, RIGHT, END_GAME, PAUSE, UNKNOWN_MOVE};
char getCharBlock(int type);

#endif
