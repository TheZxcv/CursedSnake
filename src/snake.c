#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>

#include "snake.h"

int **grid;
Settings sets;


int main(int argc, char *argv[]) {
	srand(time(NULL));

	if(load_config(&sets) == -1) {
		printf("Error while reading config file.\n");
		sets.rows = DEFAULT_ROWS;
		sets.cols = DEFAULT_COLS;
	} else
		printf("read config file:\n\trows=%d\n\tcols=%d\n", sets.rows, sets.cols);

	grid = calloc(sets.cols, sizeof(int *));
	int i;
	for(i = 0; i < sets.cols; ++i)
		grid[i] = calloc(sets.rows, sizeof(int));

	point head = {sets.cols/2, sets.rows/2};
	point tail = {sets.cols/2, sets.rows/2+1};
	list *snake = new_list(&head, &tail);

	grid[head.x][head.y] = SNAKE;
	grid[tail.x][tail.y] = SNAKE;
	place_walls(grid);
	place_fruit(grid);

	initVideo();
	initInput();

	int input, lastInput = UP;
	int loop = TRUE, alive = TRUE, pause = TRUE, ret;
	while(loop && alive) {
		drawGrid(grid, sets.rows, sets.cols);
		ret = getInputPlayer(&input);
		if(ret == 0)
			input = lastInput;
		if(input == END_GAME) {
			loop = 0;
			continue;
		} else if(input == PAUSE) {
			pause = !pause;
			input = NONE;
		}
		if(!pause) {
			lastInput = checkInput(input, lastInput);
			alive = updateGrid(grid, snake, lastInput);
		}
		if(100000 - snake->length*5000 > 10000)
			usleep(100000 - snake->length*5000);
		else
			usleep(10000);
	}

	closeVideo();
	return EXIT_SUCCESS;
}

void place_fruit(int **grid) {
	int i, j;
	do {
		i = rand()%sets.cols;
		j = rand()%sets.rows;
	} while(grid[i][j] != FLOOR);
	grid[i][j] = FRUIT;
}

void place_walls(int **grid) {
	int i, max = (sets.rows > sets.cols) ? sets.rows : sets.cols;
	for(i=0; i < max; ++i) {
		if(i < sets.cols) {
			grid[i][0] = WALL;
			grid[i][sets.rows-1] = WALL;
		}
		if(i < sets.rows) {
			grid[0][i] = WALL;
			grid[sets.cols-1][i] = WALL;
		}
	}
}

int checkInput(int input, int lastInput) {
	switch(input) {
		case UP:
			if(lastInput != DOWN)
				return input;
			break;
		case DOWN:
			if(lastInput != UP)
				return input;
			break;
		case LEFT:
			if(lastInput != RIGHT)
				return input;
			break;
		case RIGHT:
			if(lastInput != LEFT)
				return input;
			break;
		case NONE:
		default:
			return lastInput;
			break;
	}
	return lastInput;
}


char getCharBlock(int type) {
	switch(type) {
		case FLOOR:
			return ' ';
		case SNAKE:
		       	return '#';
		case FRUIT:
			return '@';
		case WALL:
			return '*';
		case UNKNOWN_TILE:
		default:
			return '?';
	}
}


int getInputPlayer(int *cmd) {
	if(hasInput()) {
		int c = rawInput();
		*cmd = inputToCommand(c);
		return 1;
	}
	return 0;
}

int inputToCommand(char c) {
	c = tolower(c);
	switch(c) {
		case 'w':
			return UP;
		case 's':
			return DOWN;
		case 'a':
			return LEFT;
		case 'd':
			return RIGHT;
		case 'e':
			return END_GAME;
		case 'p':
			return PAUSE;
		default:
			return NONE;
	}
}

int updateGrid(int **grid, list *snake, int input) {
	point p = {snake->head->p->x, snake->head->p->y};
	switch(input) {
		case UP:
//			if(snake->head->p->y > 0)
				p.y--;
			break;
		case DOWN:
//			if(snake->head->p->y < sets.rows-1)
				p.y++;
			break;
		case LEFT:
//			if(snake->head->p->x > 0)
				p.x--;
			break;
		case RIGHT:
//			if(snake->head->p->x < sets.cols-1)
				p.x++;
			break;
		case NONE:
		default:
			break;
	}

	if(grid[p.x][p.y] == WALL)
	       return 0;
	else if(grid[p.x][p.y] == SNAKE)
	       return 0;
	else if(grid[p.x][p.y] != FRUIT) {
		grid[snake->tail->p->x][snake->tail->p->y] = FLOOR;
		remove_from_tail(snake);
	} else
		place_fruit(grid);

	add_on_head(snake, &p);
	grid[p.x][p.y] = SNAKE;

	return 1;
}
