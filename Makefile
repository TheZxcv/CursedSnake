CC=gcc
CFLAGS=-c -Wall -Iinclude/
LDFLAGS=-lncurses -lSDL2 -lconfig
SOURCES=$(wildcard src/*.c)
OBJECTS=$(SOURCES:.c=.o)
ifdef ncurses
	SOURCES+=$(wildcard src/ncurses/*.c)
else
	SOURCES+=$(wildcard src/sdl/*.c)
endif
BIN=snake

all: $(SOURCES) $(BIN)

.PHONY:	SDL, ncurses

$(BIN): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf $(BIN) $(OBJECTS)

