#include "snake_input.h"

#include <SDL2/SDL.h>

int last;
SDL_Event event;

void initInput(void) {
	last = EOF;
}

int rawInput(void) {
	char c;
	while(SDL_PollEvent(&event) != 0) {
		if(event.type == SDL_KEYDOWN) {
			switch(event.key.keysym.sym) {
				case SDLK_w:
					c = 'w';
					break;
				case SDLK_a:
					c = 'a';
					break;
				case SDLK_s:
					c = 's';
					break;
				case SDLK_d:
					c = 'd';
					break;
				case SDLK_p:
					c = 'p';
					break;
				case SDLK_e:
					c = 'e';
					break;
				default:
					c = '?';
					break;
			}
			if(c != last)
				return (last = c);
		}
	}
	return EOF;
}

int hasInput(void) {
	return TRUE;
}
