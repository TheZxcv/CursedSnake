#include "snake_list.h"

list *new_list(point *h, point *t) {
	list *l = malloc(sizeof(list));
	l->head = malloc(sizeof(struct node));
	l->tail = malloc(sizeof(struct node));

	l->head->left = NULL;
	l->tail->left = l->head;

	l->head->p = malloc(sizeof(point));
	l->head->p->x = h->x;
	l->head->p->y = h->y;

	l->tail->p = malloc(sizeof(point));
	l->tail->p->x = t->x;
	l->tail->p->y = t->y;
	
	l->length = 2;

	return l;
}

void add_on_head(list *l, point *p) {
	struct node *n = malloc(sizeof(struct node));
	n->p = malloc(sizeof(point));
	n->p->x = p->x;
	n->p->y = p->y;
	
	l->head->left = n;
	l->head = n;
	l->length++;
}

void remove_from_tail(list *l) {
	struct node *old = l->tail;
	l->tail = old->left;

	free_node(old);
	l->length--;
}

void free_node(struct node *n) {
	free(n->p);
	free(n);
}

/*void add_node_on_tail(list *l, point *p) {
	struct node *n = malloc(sizeof(struct node));
	n->p = malloc(sizeof(point));
	n->p->x = p->x;
	n->p->y = p->y;
	n->right = NULL;

	l->tail->right = n;
	l->tail = n;
}*/

